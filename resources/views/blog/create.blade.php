@extends('pages.masterLayout')



@section('title', 'Contact')
@section('stylesheet')
	{!! Html::style('css/parsley.css') !!}
@endsection


@section('content')
	

	{!! Form::open(['action' => 'BlogController@store', 'data-parsley-validate' => '']) !!}

		{{ Form::label('post_title', 'Post Title', array('class' => 'h4', 'style'=>'margin-top:10px')) }}
		{{ Form::text('post_title', null, array('placeholder' => 'Post Title', 'class' => 'form-control', 'required' => '', 'minlength' => '4', 'maxlength' => '30')) }}

    	<h4>{{ Form::label('message', 'Message', array('class' => 'h4', 'style'=>'margin-top:10px')) }}</h4>
		{{ Form::textarea('message', null, array('placeholder' => 'Message here ........', 'class' => 'form-control', 'required' => '', 'minlength' => '10', 'maxlength' => '255')) }}

		{{ Form::submit('Create New Post', array('class' => 'btn btn-success btn-lg btn-block', 'style'=>'margin-top:20px')) }}
	

	

	{!! Form::close() !!}

@endsection



@section('js_script')
	{!! Html::script('js/parsley.js') !!}
@endsection