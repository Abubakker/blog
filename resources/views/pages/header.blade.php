<head>
	<title>@yield('title')</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	
	@yield('stylesheet')



	<div class="container-fluid">
	@section('manu')



		<!-- Static navbar -->
	      <nav class="navbar navbar-default">

	        <div class="container-fluid">

	          <div class="navbar-header">
	            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	              <span class="sr-only"></span>
	              <span class="icon-bar"></span>
	              <span class="icon-bar"></span>
	              <span class="icon-bar"></span>
	            </button>
	            <a class="navbar-brand" href="/">Blog</a>
	          </div>

	          <div id="navbar" class="navbar-collapse collapse">
	            <ul class="nav navbar-nav">
	              <li class="active"><a href="/">Home</a></li>
	              <li><a href="/about">About</a></li>
	              <li><a href="/contact">Contact</a></li>
	              <li class="dropdown">
	                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Others<span class="caret"></span></a>	                
	              </li>
	            </ul>
	            <ul class="nav navbar-nav navbar-right">
	              <li class="active"><a href="/">Default <span class="sr-only">(current)</span></a></li>
	              <li><a href="/">Static top</a></li>
	              <li><a href="/">Fixed top</a></li>
	            </ul>
	          </div><!--/.nav-collapse -->

	        </div><!--/.container-fluid -->

	      </nav>



	@show
</head>