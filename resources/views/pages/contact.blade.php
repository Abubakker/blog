@extends('pages.masterLayout')



@section('title', 'Contact')



@section('content')

	<form action="{{url('')}}" method="post">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		  
		  <div class="form-group">
		    <label for="exampleInputEmail">Email address</label>
		    <input type="email" name="email" class="form-control" id="exampleInputEmail" placeholder="example@gmail.com">
		  </div>
		  <div class="form-group">
		    <label for="post_title">Post Title</label>
		    <input type="text" name="post_title" class="form-control" id="post_title" placeholder="Post Title">
		  </div>
		  <div class="form-group">
		    <label for="message">Message</label>
		    <textarea class="form-control" id="message" rows="5" placeholder="Message type here ......... "></textarea>		    
		  </div>
		  <div class="form-group">
		    <label for="exampleInputFile">Your Image</label>
		    <input type="file" name="image" id="exampleInputFile">
		    <p class="help-block">type.jpg or type.png</p>
		  </div>
		  <div class="checkbox">
		    <label>
		      <input type="checkbox">I agree
		    </label>
		  </div>


		  <button type="submit" class="btn btn-success">Submit</button>

	</form>	

@endsection