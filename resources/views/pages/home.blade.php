@extends('pages.masterLayout')



@section('title', 'Home')



@section('content')

      

      
    <div class="row">
      	<div class="col-md-12">	
	      <div class="jumbotron">
	        <h1>Welcome to My blog !</h1>
	        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	        <p>
	        	<a class="btn btn-lg btn-primary" href="/" role="button">Popular Post &raquo;</a>
	        </p>
	      </div>
	    </div>
	</div>


	<div class="row">
      	<div class="col-md-8">		 
			<div class="">
		    	<h1>Post Title here</h1>
		        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		        <p>
		          <a class="btn btn-lg btn-primary" href="/" role="button">Read More &raquo;</a>
		        </p>
		    </div><hr>
		</div>


      	<div class="col-md-3 col-md-offset-1">			
	    	<h1>Side Bar</h1>
	        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	        tempor incididunt ut labore et dolore magna aliqua.</p>
	        <p>
	          <a class="btn btn-lg btn-primary" href="/" role="button">Read More &raquo;</a>
	        </p>		    
		</div>
	</div>    
	



@endsection