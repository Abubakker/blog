<!DOCTYPE html>
<html>
@include('pages.header')
	@section('title')
		@yield('title')
	@stop

	@section('stylesheet')
		@yield('stylesheet')
	@stop

<body>
	<div class="container">
		@yield('content')

		@include('pages.message')


		@include('pages.footer')
		@section('js_script')
			@yield('js_script')
		@stop


	</div>	
	
</body>
</html>